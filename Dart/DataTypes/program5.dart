main() {
  double x = 10.5;
  print(x);
  print(x.runtimeType);

  num y = 20.5;
  print(y);
  print(y.runtimeType);

  y = 20;
  print(y);

  x = 35;
  print(x);
}
