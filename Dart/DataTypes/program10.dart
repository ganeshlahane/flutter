void main() {
  dynamic x = 10;
  dynamic y = 20;

  print(x.runtimeType);
  print(y.runtimeType);

  x = 20.5;
  y = "Ganesh";

  print(x.runtimeType);
  print(y.runtimeType);
}
