void main() {
  const int x = 10;
  print(x);

  const int z = 20;
  print(z);

  const y = 30;
  print(y);

  //x = 50; Error: Can't assign to the const variable 'x'.
}
