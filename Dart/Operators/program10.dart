// << = Left Shift
// >> = Right Shift

void main() {
  int x = 43;
  int y = 72;

  print(x);
  print(x << 3);

  print(y);
  print(y >> 4);
}
